package ru.t1.vlvov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.command.data.AbstractDataCommand;
import ru.t1.vlvov.tm.command.data.DataBackupLoadCommand;
import ru.t1.vlvov.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    private final Bootstrap bootstrap;

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP)))
            bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        load();
        es.scheduleAtFixedRate(this::save, 0, 3, TimeUnit.SECONDS);
    }

}
